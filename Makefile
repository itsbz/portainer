start: pull up clear

init:
	@cp .env.example .env

pull:
	@docker-compose pull

up:
	@docker-compose up -d

down:
	@docker-compose down

stop:
	@docker-compose stop

clear:
	@docker system prune -af --volumes

